# capacitacionFundamentos

## Getting started

First:

Clone the project in this link:

https://gitlab.com/delcarpio.rene/capacitacionfundamentos

Second:

Make sure you have installed the folowing packages.

- Microsoft.EntityFrameworkCore
- Microsoft.EntityFrameworkCore.SqlServer
- Microsoft.EntityFrameworkCore.Tools

All this packages must be installed in the version 5.0.17

Third:

Go to the path where you download the project, open the terminal in that place and type this command in the terminal

dotnet watch run

You should be able to run the project and the browser will open automatically and show swagger. When you test the endpoint this should show just the DTO information.
