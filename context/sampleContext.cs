using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using capacitacionfundamentos.models;
using Microsoft.EntityFrameworkCore;

namespace capacitacionfundamentos.context
{
    public class sampleContext : DbContext
    {
        public sampleContext(DbContextOptions<sampleContext> options) : base(options)
        {

        }

        public DbSet<Student> Students { get; set; }
    }
}