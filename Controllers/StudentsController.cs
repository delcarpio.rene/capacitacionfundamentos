using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using capacitacionfundamentos.context;
using capacitacionfundamentos.Dto;
using capacitacionfundamentos.models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace capacitacionfundamentos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly sampleContext _context;

        public StudentsController(sampleContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IQueryable<StudentDto> GetStudents()
        {
            var studentsList = from s in _context.Students select new StudentDto()
            {
                Id = s.Id,
                Name = s.Name
            };

            return studentsList;
        }

        [HttpPost]
        public async Task<IActionResult> CreateStudent([FromBody] Student student)
        {
            if(student == null)
            {
                return BadRequest(ModelState);
            }
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _context.AddAsync(student);
            await _context.SaveChangesAsync();

            return Ok(student);
        }
    }
}